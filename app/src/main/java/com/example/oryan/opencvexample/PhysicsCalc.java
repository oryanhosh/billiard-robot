package com.example.oryan.opencvexample;

import org.opencv.core.Mat;
import org.opencv.core.Point;

import java.util.ArrayList;
import java.util.List;


/**
 * This class contains physics formulas and algorithms
 * */
class PhysicsCalc {

    /**
     * Function return list of all good options by angel
     * input: list of balls, white ball, hole
     * output: list of balls
     * */
    static List<double[]> getAllGoodOptionsToHoleAndWhite(List<double[]> points, Point whiteBall, Point hole){
        List<double[]> list = new ArrayList<>();
        for (double[] p : points){
            double angel = calcAngelBetween3Points(whiteBall, hole, p);
            if(angel  > Constants.MAX_ANGEL){
                list.add(new double[]{p[0], p[1], angel, hole.x, hole.y});
            }
        }
        return list;
    }

    /**
     * Function return list with all possible balls to the hole with [2] == 1
     * and also all other balls with [2] == 0
     * input: list of balls, white ball and hole
     * output: list of balls
     * */
    static List<double[]> getAllPossibleBallsToHole(Mat circles, int whiteIndex, Point hole) {
        List<double[]> points = new ArrayList<>();
        for (int i = 0; i < circles.cols(); i++)
        {
            if(whiteIndex != i) //all balls except white
            {
                Point point = MainActivity.getPointByIndex(circles, i);
                boolean touch = false;
                for (int j = 0; j < circles.cols(); j++){
                    if(j != i && j != whiteIndex) //if ball not white or current
                    {   //if ball can not go to hole
                        if(ballCanNotEnterHole(hole.x,hole.y,point.x,point.y,circles.get(0,j)[0],circles.get(0,j)[1])){
                            touch = true;
                            break;
                        }
                    }
                }
                if(!touch){ //ball can enter hole
                    double t = -2*Constants.BALL_RADIUS/(Math.sqrt(Math.pow((hole.x-point.x), 2) + Math.pow((hole.y-point.y), 2)));
                    points.add(new double[]{(1-t)*point.x+t*hole.x,(1-t)*point.y+t*hole.y, 1});
                }
                else{
                    points.add(new double[]{point.x, point.y, 0});
                }
            }
        }
        return points;
    }

    /**
    * Function check if (x1, x2) can go to (x3, y3) without touch (x2,y2)
    * input: x and y of 3 balls
    * output: true / false
    * */
    private static boolean ballCanNotEnterHole(double x1, double y1, double x2, double y2, double x3, double y3){
        double dis = Math.abs((x2- x1)*(y1 - y3) - (x1 - x3)*(y2 - y1))/
                Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
        if(dis < Constants.BALL_RADIUS*2){
            double dis1 = Math.sqrt(Math.pow((x1-x3), 2) + Math.pow((y1-y3), 2));
            double dis2 = Math.sqrt(Math.pow((x1-x2), 2) + Math.pow((y1-y2), 2));
            return dis1 < dis2 &&  checkIfInside(x1,y1,x2,y2,x3,y3);
        }
        return false;
    }

    /**
     * Function return all the ball who can go to the white ball
     * input: white ball, all other balls
     * output: list of balls
     * */
    static List<double[]> getAllPossiblePointsToWhite(Point white, List<double[]> circles) {
        List<double[]> balls = new ArrayList<>();
        for (int i = 0; i < circles.size(); i++)
        {
            double vCircle[] = circles.get(i);
            if (vCircle[2] != 0) //if ball can go to hole
            {
                double x = vCircle[0];
                double y = vCircle[1];
                boolean touch = false;
                for (int j = 0; j < circles.size(); j++){
                    if(j != i)
                    {   //if ball can not go to white
                        if(ballCanNotEnterHole(white.x,white.y,x,y,circles.get(j)[0],circles.get(j)[1])){
                            touch = true;
                            break;
                        }
                    }
                }
                if(!touch){ //ball can go to white
                    balls.add(new double[]{x,y});
                }
            }
        }
        return balls;
    }

    /**
     * Function check if ball inside the rectangle of the other 2 balls
     * input: 2 balls for the rect, ball for check
     * output: true / false
     * */
    private static boolean checkIfInside(double x1, double y1, double x2, double y2, double x, double y) {
        int a = 2;
        if (x1 < x2 && y1 < y2) //check for arc 1
        {
            return x > x1-a*Constants.BALL_RADIUS && x < x2+a*Constants.BALL_RADIUS &&
                    y > y1-a*Constants.BALL_RADIUS && y < y2+a*Constants.BALL_RADIUS;
        }
        else if (x1 > x2 && y1 > y2)//check for arc 3
        {
            return x > x2-a*Constants.BALL_RADIUS && x < x1+a*Constants.BALL_RADIUS &&
                    y > y2-a*Constants.BALL_RADIUS && y < y1+a*Constants.BALL_RADIUS;
        }
        else if (x1 < x2 && y1 > y2)//check for arc 4
        {
            return x > x1-a*Constants.BALL_RADIUS && x < x2+a*Constants.BALL_RADIUS &&
                    y > y2-a*Constants.BALL_RADIUS && y < y1+a*Constants.BALL_RADIUS;
        }
        else //(x1 > x2 && y1 < y2) //check for arc 2
        {
            return x > x2-a*Constants.BALL_RADIUS && x < x1+a*Constants.BALL_RADIUS &&
                    y > y1-a*Constants.BALL_RADIUS && y < y2+a*Constants.BALL_RADIUS;
        }
    }

    /**
     * Function return the whitest ball on the frame
     * input: list of balls, frame
     * output: the whitest ball
     * */
     static int getWhiteBall(Mat circles, Mat frameMat){
        int maxSum = 0;
        int index = Constants.NOT_FOUND;
        for (int i = 0; i < circles.cols(); i++){
            int x = (int)circles.get(0,i)[1];
            int y = (int)circles.get(0,i)[0];
            //check if the ball is outside the board
            if(x > Constants.FRAME_HEIGHT-Constants.BALL_RADIUS*2 || y > Constants.FRAME_WIDTH-Constants.BALL_RADIUS*2 ||
                    x < Constants.BALL_RADIUS*2 || y < Constants.BALL_RADIUS*2)
            {
                continue; //skip to the next ball
            }
            int add = Constants.BALL_RADIUS/3;
            double pixelColor0[] = frameMat.get(x,y);
            double pixelColor1[] = frameMat.get(x+add,y+add);
            double pixelColor2[] = frameMat.get(x-add,y-add);
            double pixelColor3[] = frameMat.get(x+add,y-add);
            double pixelColor4[] = frameMat.get(x-add,y+add);
            double pixelColor5[] = frameMat.get(x,y+add);
            double pixelColor6[] = frameMat.get(x,y-add);
            double pixelColor7[] = frameMat.get(x-add,y);
            double pixelColor8[] = frameMat.get(x+add,y);
            //sum all the R G B values of the ball
            int sum = ((int)pixelColor0[0] + (int)pixelColor0[1] +(int)pixelColor0[2] +
                    (int)pixelColor1[0] + (int)pixelColor1[1] +(int)pixelColor1[2] +
                    (int)pixelColor2[0] + (int)pixelColor2[1] +(int)pixelColor2[2] +
                    (int)pixelColor3[0] + (int)pixelColor3[1] +(int)pixelColor3[2] +
                    (int)pixelColor4[0] + (int)pixelColor4[1] +(int)pixelColor4[2] +
                    (int)pixelColor5[0] + (int)pixelColor5[1] +(int)pixelColor5[2] +
                    (int)pixelColor6[0] + (int)pixelColor6[1] +(int)pixelColor6[2] +
                    (int)pixelColor7[0] + (int)pixelColor7[1] +(int)pixelColor7[2] +
                    (int)pixelColor8[0] + (int)pixelColor8[1] +(int)pixelColor8[2]);
            if(maxSum < sum){
                maxSum = sum;
                index = i;
            }
        }
        return index;
    }

    /**
     * Function calculate angel between 3 points
     * input: 3 points
     * output: angel
     * */
    private static double calcAngelBetween3Points(Point whiteBall, Point hole, double[] p){
        double AB = Math.sqrt(Math.pow(hole.x - p[0], 2) + Math.pow(hole.y - p[1], 2));
        double AC = Math.sqrt(Math.pow(whiteBall.x - p[0], 2) + Math.pow(whiteBall.y - p[1], 2));
        double BC = Math.sqrt(Math.pow(whiteBall.x - hole.x, 2) + Math.pow(whiteBall.y - hole.y, 2));
        double ratio = (AB * AB + AC * AC - BC * BC) /( 2 * AC * AB);
        return Math.acos(ratio)*(180/Math.PI);
    }
}
