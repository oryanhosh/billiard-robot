package com.example.oryan.opencvexample;

/**
 * This class contains the bluetooth protocol ans some constants of bluetooth
 * */
class BluetoothProtocol {

    //message protocol
    static final int START_GAME                 = 1; //app -> robot
    static final int OPENING_STRIKE_DONE        = 2; //robot -> app
    static final int UPDATE_SCORE_AND_STRIKE    = 3; //app -> robot
    static final int STIKE_DONE                 = 4; //robot -> app
    static final int END_GAME                   = 5; //app -> robot

    //global protocol for handling ev3 brick messages
    static final int MESSAGE_STATE_CHANGE = 1;
    static final int MESSAGE_READ = 2;
    static final int MESSAGE_WRITE = 3;
    static final int MESSAGE_DEVICE_NAME = 4;
    static final int MESSAGE_TOAST = 5;

    // Key names received from the BluetoothEV3Service Handler
    static final String DEVICE_NAME = "device_name";
    static final String TOAST = "toast";


}
