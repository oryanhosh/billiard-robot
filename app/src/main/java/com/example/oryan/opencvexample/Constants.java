package com.example.oryan.opencvexample;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * This class contains constants for openCV
 * */
class Constants {
    static final int    BALL_RADIUS     = 20;
    static final int    MAX_ANGEL       = 105;
    static final Scalar WHITE_SCALAR    = new Scalar(255,255,255);
    static final Scalar BLACK_SCALAR    = new Scalar(0,0,0);
    static final Scalar RED_SCALAR      = new Scalar(255,0,0);
    static final Scalar GREEN_SCALAR    = new Scalar(0,255,0);
    static final int    NOT_FOUND       = -1;
    static final int    PADDING         = 45;

    static ArrayList HOLES_LIST;
    static int FRAME_WIDTH;
    static int FRAME_HEIGHT;

    /**
    * Function init the frame constants
    * input: frame
    * output: noun
    * */
    static void initFrameSettings(Mat frame)
    {
        Point corner1, corner2, corner3, corner4, middle1, middle2;

        FRAME_WIDTH = frame.width();
        FRAME_HEIGHT = frame.height();
        corner1 = new Point(0, PADDING);
        corner2 = new Point(frame.width(), PADDING);
        corner3 = new Point(frame.width(), frame.height()-PADDING);
        corner4 = new Point(0, frame.height()-PADDING);
        middle1 = new Point(frame.width()/2, PADDING);
        middle2 = new Point(frame.width()/2, frame.height()-PADDING);
        HOLES_LIST = new ArrayList<>(Arrays.asList(corner1, corner2, corner3, corner3, corner4, middle1, middle2));
    }

}
