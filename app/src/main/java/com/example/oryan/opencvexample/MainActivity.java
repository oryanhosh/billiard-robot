package com.example.oryan.opencvexample;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceView;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
* Main activity - hold the bluetooth and openCV main functions
* */
public class MainActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2{

    private CameraBridgeViewBase cameraBridgeViewBase;
    private BaseLoaderCallback baseLoaderCallback;
    private TextView textViewOnScreen;
    private Mat frameMat = null;
    private Mat frameMatGray2 = null;
    private Mat frameMatGray = null;
    private Mat tresh = null;
    private Mat diffMat = null;
    private Mat empty = null;
    private int ballsNum = 0;
    private boolean firstFrameRun = true;
    private boolean waitingMode = true;

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int REQUEST_CAMERA_PERMISSION = 3;

    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;

    // Member object for the EV3 services
    private BluetoothEV3Service mEV3Service = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            finish();
            return;
        }

        textViewOnScreen = findViewById(R.id.textView);

        cameraBridgeViewBase = findViewById(R.id.camView);
        cameraBridgeViewBase.setVisibility(SurfaceView.VISIBLE);
        cameraBridgeViewBase.setCvCameraViewListener(this);

        baseLoaderCallback = new BaseLoaderCallback(this) {
            @Override
            public void onManagerConnected(int status) {
                switch (status){
                    case BaseLoaderCallback.SUCCESS:
                        cameraBridgeViewBase.enableView();
                        break;
                    default:
                        super.onManagerConnected(status);
                        break;
                }
            }
        };
        runTextViewThread();
    }

    /**
    * Function starts on every new camera frame
    * */
    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        if(!waitingMode)
        {
            frameMatGray = inputFrame.gray();
            frameMat = inputFrame.rgba();

            if(firstFrameRun){ //if first run
                empty = Mat.zeros(Constants.FRAME_WIDTH, Constants.FRAME_HEIGHT, CvType.CV_8U);
                Constants.initFrameSettings(frameMat);
                firstFrameRun = false;
            }
            if(frameMatGray2!=null)
            {
                diffMat = empty;
                tresh = empty;
                Core.absdiff(frameMatGray, frameMatGray2, diffMat);
                Imgproc.threshold(diffMat, tresh, 20, 255, Imgproc.THRESH_BINARY);
                Imgproc.blur(tresh, tresh, new Size(10, 10));
                Imgproc.threshold(tresh, tresh, 20, 255, Imgproc.THRESH_BINARY);
                searchForMovement();
            }
            frameMatGray2 = frameMatGray;
            Mat circles = getAllCirclesInFrame(); //list of balls

            //get white ball index
            int whiteBallIndex = PhysicsCalc.getWhiteBall(circles, frameMat);

            Point[] finalTarget = null;

            if(whiteBallIndex != Constants.NOT_FOUND){
                //get white ball
                Point whiteBall = getPointByIndex(circles, whiteBallIndex);

                List<double[]> shots = new ArrayList<>();
                for(Object hole : Constants.HOLES_LIST){ //for every hols
                    List<double[]> possibleToHole = PhysicsCalc.getAllPossibleBallsToHole(circles, whiteBallIndex, (Point) hole);
                    List<double[]> possibleToWhite = PhysicsCalc.getAllPossiblePointsToWhite(whiteBall, possibleToHole);
                    List<double[]> onlyGoodOptions = PhysicsCalc.getAllGoodOptionsToHoleAndWhite(possibleToWhite, whiteBall, (Point) hole);
                    shots.addAll(onlyGoodOptions); //add good shots to the list
                }
                //draw shots, board and balls on the screen
                finalTarget = Painter.drawInformationOnScreen(shots, whiteBall, circles, frameMat);
            }
            Painter.drawCornersOnScreen(frameMat); //draw board
            if(finalTarget != null){
                sendMessageToBrick(finalTarget);
            }
            return frameMat;
        }
        else{
            if(frameMat!=null)
                return frameMat;
            return inputFrame.rgba();
        }
    }

    /**
    * Function calculate message and send it to the brick
     * input: array of points = {whiteBall, targetBall}
     * output: noun
    * */
    private void sendMessageToBrick(Point[] points) {
        waitingMode = true;
        double ang = calculateAngelBetweenBalls(points);
        Log.d("oryan", "angel:" + String.valueOf(ang));
         mEV3Service.EV3.sendMailBox(3f);
        mEV3Service.EV3.sendMailBox((float) ang);
        mEV3Service.EV3.sendMailBox((float) points[0].x);
        mEV3Service.EV3.sendMailBox((float) points[0].y);
        //finishGame();
    }

    /**
     * Function exit the app
     * input: noun
     * output: noun
     * */
    public void finishGame()
    {
        mEV3Service.EV3.sendMailBox(BluetoothProtocol.END_GAME);
        mEV3Service = null;
 //       Toast.makeText(this, "closing in 5 seconds", Toast.LENGTH_SHORT).show();
 /*       new CountDownTimer(5000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                // do something after 1s
            }

            @Override
            public void onFinish() {
                finish();
            }}.start();*/
        return;
    }


    public double calculateAngelBetweenBalls(Point[] points){
        return (Math.toDegrees(Math.atan((points[0].y - points[1].y) / (points[0].x - points[1].x))) + 90);
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
    }

    @Override
    public void onCameraViewStopped() {
        if(frameMat!=null)
            frameMat.release();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(cameraBridgeViewBase!=null){
            cameraBridgeViewBase.disableView();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!OpenCVLoader.initDebug()){
            Toast.makeText(this, "OpenCV problem", Toast.LENGTH_SHORT).show();
        }else{
            baseLoaderCallback.onManagerConnected(BaseLoaderCallback.SUCCESS);
        }
        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mEV3Service != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mEV3Service.getState() == BluetoothEV3Service.STATE_NONE) {
                // Start the Bluetooth services
                mEV3Service.start();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(cameraBridgeViewBase!=null){
            cameraBridgeViewBase.disableView();
        }
        if (mEV3Service != null)
            mEV3Service.stop();
    }

    @Override
    public void onBackPressed() {
        //disable BackPress button by Override
    }

    @Override
    protected void onStart() {
        super.onStart();

        RequestCameraPermission();

        // If BlueTooth is not on, request that it be enabled.
        // setupApp() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled())
        {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the session
        }
        else
        {
            if (mEV3Service == null)
            {
                setupApp();
            }
        }
    }

    /**
    * Function starts bluetooth service and DeviceListActivity
    * */
    private void setupApp() {
        // Initialize the BluetoothEV3Service to perform bluetooth connections
        mEV3Service = new BluetoothEV3Service(this, mHandler);

        if (mEV3Service.getState() == BluetoothEV3Service.STATE_NONE){
            //start device list activity
            Intent serverIntent = new Intent(this, DeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
        }
    }

    /**
    * Function mark the movement
     * input: noun
     * output: noun
    * */
    void searchForMovement(){
        Mat hierarchy = Mat.zeros(Constants.FRAME_WIDTH, Constants.FRAME_HEIGHT, CvType.CV_8U);
        List<MatOfPoint> contours = new ArrayList<>();
        Imgproc.findContours(tresh,contours,hierarchy,Imgproc.RETR_EXTERNAL,Imgproc.CHAIN_APPROX_SIMPLE );// retrieves external contours
        int[] theObject = new int[2];
        if(contours.size()>0){
            double maxVal = 0;
            int maxValIdx = 0;
            for (int contourIdx = 0; contourIdx < contours.size(); contourIdx++)
            {
                double contourArea = Imgproc.contourArea(contours.get(contourIdx));
                if (maxVal < contourArea)
                {
                    maxVal = contourArea;
                    maxValIdx = contourIdx;
                }
            }
            Rect objectBoundingRectangle =  Imgproc.boundingRect(contours.get(maxValIdx));
            int xpos = objectBoundingRectangle.x+objectBoundingRectangle.width/2;
            int ypos = objectBoundingRectangle.y+objectBoundingRectangle.height/2;
            theObject[0] = xpos;
            theObject[1] = ypos;
        }
        int x = theObject[0];
        int y = theObject[1];
        Imgproc.circle(frameMat,new Point(x,y),Constants.BALL_RADIUS,new Scalar(0,255,0),2);
        Imgproc.line(frameMat,new Point(x,y),new Point(x,y-Constants.BALL_RADIUS-2),new Scalar(0,255,0),2);
        Imgproc.line(frameMat,new Point(x,y),new Point(x,y+Constants.BALL_RADIUS+2),new Scalar(0,255,0),2);
        Imgproc.line(frameMat,new Point(x,y),new Point(x-Constants.BALL_RADIUS-2,y),new Scalar(0,255,0),2);
        Imgproc.line(frameMat,new Point(x,y),new Point(x+Constants.BALL_RADIUS+2,y),new Scalar(0,255,0),2);
    }

    /**
     * Function return mat of all the circles in the frame
     * input: noun
     * output: list of circles
     * */
    private Mat getAllCirclesInFrame(){
        Mat grayMat = new Mat();
        Mat circles = new Mat();
        Imgproc.cvtColor( frameMat, grayMat, Imgproc.COLOR_BGR2GRAY); //convert to gray
        Imgproc.HoughCircles( grayMat, circles, Imgproc.CV_HOUGH_GRADIENT,
                1, Constants.BALL_RADIUS*2-5, 100, 10, Constants.BALL_RADIUS, Constants.BALL_RADIUS);
        ballsNum = circles.cols();
        return circles;
    }

    /**
     * Function return point from list by index
     * input: list of circles and index
     * output: point
     * */
    static Point getPointByIndex(Mat circles, int index){
        return new Point(circles.get(0,index)[0], circles.get(0,index)[1]);
    }

    private void runTextViewThread() {
        new Thread() {
            public void run() {
                while (true) {
                    try {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                textViewOnScreen.setText(String.valueOf(ballsNum) + " balls on the board");
                                if(ballsNum == 16)
                                    textViewOnScreen.setTextColor(Color.GREEN);
                                else
                                    textViewOnScreen.setTextColor(Color.WHITE);
                            }
                        });
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode)
        {
            case REQUEST_CONNECT_DEVICE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    // Get the device MAC address
                    String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                    // Attempt to connect to the device
                    mEV3Service.connect(device);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a session
                    setupApp();
                } else {
                    //Bluetooth was not enabled. Leaving EV3 Numeric.
                    finish();
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED)
            {
                Toast.makeText(this, "camera permission is needed", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void RequestCameraPermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
        }
    }

    /**
    * Function check the message from the brick
     * input: message from brick
     * output: noun
    * */
    private void handleMessageFromBrick(byte[] msg) {
        String receivedMsg = null;
        try {
            receivedMsg = new String(Arrays.copyOfRange(msg, 13,17),"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if(receivedMsg== null || !receivedMsg.equals("done")){
            Toast.makeText(this, "unrecognized message came from brick", Toast.LENGTH_SHORT).show();
        }
        else{
            waitingMode = false; //start calculate the next hit
        }
    }

    /**
     * The Handler that gets information back from the BluetoothEV3Service
     * */
    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case BluetoothProtocol.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothEV3Service.STATE_CONNECTED:
                            Toast.makeText(MainActivity.this, "device connected", Toast.LENGTH_SHORT).show();
                            break;
                        case BluetoothEV3Service.STATE_CONNECTING:
                            break;
                        case BluetoothEV3Service.STATE_NONE:
                            break;
                    }
                    break;
                case BluetoothProtocol.MESSAGE_READ: //message from brick
                    handleMessageFromBrick((byte[])msg.obj);
                    break;
                case BluetoothProtocol.MESSAGE_DEVICE_NAME: //fist message from brick
                    mEV3Service.EV3.sendMailBox((float) BluetoothProtocol.START_GAME);
                    break;
                case BluetoothProtocol.MESSAGE_TOAST: //error message
                    Toast.makeText(MainActivity.this, msg.getData().getString(BluetoothProtocol.TOAST), Toast.LENGTH_SHORT).show();
                    break;
                default:
                    break;
            }
        }
    };
}
