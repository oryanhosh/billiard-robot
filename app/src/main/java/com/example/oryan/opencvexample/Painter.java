package com.example.oryan.opencvexample;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

/**
* This class paint info on the screen
* */
class Painter {

    /**
     * Function draw all information on screen
     * input: list of shots, white ball, list of circles
     * output: array of points = {whiteBall, targetBall}
     * */
    static Point[] drawInformationOnScreen(List<double[]> shots, Point whiteBall, Mat circles, Mat frameMat){
        double maxAngel = 0;
        double[] bestBall = null;
        if(!shots.isEmpty()){
            for(double[] shot : shots){ //get best shot
                Imgproc.line(frameMat, new Point(shot[0],shot[1]), whiteBall, Constants.WHITE_SCALAR, 1);
                Imgproc.line(frameMat, new Point(shot[0],shot[1]), new Point(shot[3],shot[4]), Constants.WHITE_SCALAR, 1);
                if(shot[2] > maxAngel){
                    maxAngel = shot[2];
                    bestBall = shot;
                }
            }
            Point targetBall = new Point(bestBall[0],bestBall[1]);
            Point hole = new Point(bestBall[3], bestBall[4]);
            //draw best shot (white to ball)
            Imgproc.line(frameMat, targetBall, whiteBall, Constants.GREEN_SCALAR, 2);
            //draw best shot (ball to hole)
            Imgproc.line(frameMat, targetBall, hole, Constants.GREEN_SCALAR, 2);
            return new Point[]{whiteBall, targetBall};
        }
        //draw center of white ball
        Imgproc.circle(frameMat, whiteBall, 3, Constants.GREEN_SCALAR, -1, 8, 0 );
        drawAllBalls(circles, frameMat); //draw all balls
        return null;
    }

    /**
     * Function draw all balls on screen
     * input: list of circles
     * output: noun
     * */
    private static void drawAllBalls(Mat circles, Mat frameMat) {
        for (int i = 0; i < circles.cols(); i++)
        {
            Imgproc.circle( frameMat, MainActivity.getPointByIndex(circles, i), Constants.BALL_RADIUS,
                    Constants.RED_SCALAR, 1, 8, 0 );
        }
    }

    /**
     * Function draw the board
     * input: noun
     * output: noun
     * */
    static void drawCornersOnScreen(Mat frameMat){
        ArrayList<Point> pointsOrdered = new ArrayList<>();
        pointsOrdered.add(new Point( Constants.FRAME_WIDTH, Constants.PADDING));
        pointsOrdered.add(new Point( Constants.FRAME_WIDTH, 0));
        pointsOrdered.add(new Point( 0, 0));
        pointsOrdered.add(new Point( 0, Constants.PADDING));
        MatOfPoint sourceMat = new MatOfPoint();
        sourceMat.fromList(pointsOrdered);
        Imgproc.fillConvexPoly(frameMat,sourceMat, Constants.BLACK_SCALAR);

        ArrayList<Point> pointsOrdered2 = new ArrayList<>();
        pointsOrdered2.add(new Point( Constants.FRAME_WIDTH, Constants.FRAME_HEIGHT));
        pointsOrdered2.add(new Point(Constants.FRAME_WIDTH, Constants.FRAME_HEIGHT-Constants.PADDING));
        pointsOrdered2.add(new Point( 0, Constants.FRAME_HEIGHT-Constants.PADDING));
        pointsOrdered2.add(new Point( 0, Constants.FRAME_HEIGHT));
        MatOfPoint sourceMat2 = new MatOfPoint();
        sourceMat2.fromList(pointsOrdered2);
        Imgproc.fillConvexPoly(frameMat,sourceMat2, Constants.BLACK_SCALAR);
    }
}
